class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = "_" * length
  end

  def take_turn
    guess = @guesser.guess(@board)
    index = @referee.check_guess(guess)
    guesser.handle_response(guess, index)
    update_board(guess, index)
  end

  def update_board(guess, index)
    index.each do |idx|
      board[idx] = guess
    end
  end

end

class HumanPlayer
end

class ComputerPlayer

  attr_reader :candidate_words, :dictionary

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    ("a".."z").to_a.sample
  end

  def handle_response(guess, index)
  @candidate_words.reject! do |word|
    delete = false

      word.chars.each_with_index do |letter, idx|
        if (letter == guess) && (!index.include?(idx))
          delete = true
          break
        elsif (letter != guess) && (index.include?(idx))
          delete = true
          break
        end
      end
      delete
  end
  end

  def guess(board)

   freq_table = freq_table(board)

   most_frequent_letters = freq_table.sort_by { |letter, count| count }
   letter, _ = most_frequent_letters.last

   letter
 end

 def check_guess(guess)
   index = []
   @secret_word.chars.each_with_index do |el, idx|
     index << idx if el == guess
   end
   index
   end

   private
   def freq_table(board)

   freq_table = Hash.new(0)
   @candidate_words.each do |word|
     board.each_with_index do |letter, index|

       freq_table[word[index]] += 1 if letter.nil?
     end
   end

    freq_table
  end
end
